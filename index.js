/*
const textFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const textLastName = document.querySelector("#txt-last-name")
// document.querySelector is use to retrieve an element from the webpage
// We can also use a class selector (".text-input")

function updateFullName() {
  
  spanFullName.innerHTML = `${textFirstName.value} ${textLastName.value}`

}

updateFullName.addEventListener("keyup", (event) => {

	return updateFullName;

} */


function updateFullName() {

  const textFirstName = document.querySelector("#txt-first-name").value;
  const textLastName = document.querySelector("#txt-last-name").value;
  
  document.querySelector('#span-full-name').textContent = `${textFirstName} ${textLastName}`;
}

document.querySelector("#txt-first-name").addEventListener('input', updateFullName);
document.querySelector("#txt-last-name").addEventListener('input', updateFullName);

